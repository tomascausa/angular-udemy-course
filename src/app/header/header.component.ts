import { Component, OnInit, OnDestroy } from '@angular/core';
import { RecipeService } from '../recipes/recipe.service';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';
import { User } from '../shared/user.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import { map } from 'rxjs/operators';
import * as AuthAction from '../auth/store/auth.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {

  private user: Subscription;
  isAuthenticated = false;

  constructor(
    private router: Router,
    private recipeService: RecipeService,
    private authService: AuthService,
    private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.user = this.store.select('auth')
    .pipe(map(authState => {
        return authState.user;
      })
    )
    .subscribe(
      user => {
        this.isAuthenticated = !!user;
      }
    );
  }

  storeRecipes() {
    this.recipeService.storeRecipes().subscribe(
      responseData => {
        console.log(responseData);
      },
      error => {
        console.log(error);
      }
    );
  }

  fetchRecipes() {
    this.recipeService.fetchRecipes().subscribe();
  }

  onLogout() {
    this.store.dispatch(new AuthAction.Logout());
  }

  ngOnDestroy() {
    this.user.unsubscribe();
  }

}
