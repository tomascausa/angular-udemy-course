import * as AuthActions from './auth.actions';
import { User } from 'src/app/shared/user.model';

export interface AuthState {
    user: User;
    authError: string;
    loading: boolean;
}

const initialState: AuthState = {
    user: null,
    authError: null,
    loading: false
};

export function authReducer(state = initialState, action: AuthActions.AuthActions) {
    switch (action.type) {
        case AuthActions.SIGN_UP_START:
            return {
                ...state,
                authError: null,
                loading: true
            };
        case AuthActions.LOGIN_START:
            return {
                ...state,
                authError: null,
                loading: true
            };
        case AuthActions.AUHTENTICATE_SUCCESS:
            const user = new User(action.payload.id, action.payload.email, action.payload.token, action.payload.tokenExpiration);
            return {
                ...state,
                user: user,
                authError: null,
                loading: false
            };
        case AuthActions.AUHTENTICATE_FAIL:
            return {
                ...state,
                user: null,
                authError: action.payload,
                loading: false
            };
        case AuthActions.LOGOUT:
            return {
                ...state,
                user: null,
                authError: null,
                loading: null
            };
        case AuthActions.CLEAR_ERROR:
            return {
                ...state,
                authError: null
            };
        default:
            return state;
    }
}
