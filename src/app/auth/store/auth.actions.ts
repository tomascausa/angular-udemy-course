import { Action } from '@ngrx/store';
import { AuthRequest } from '../auth.model';

export const SIGN_UP_START = '[Auth] Sign Up Start';
export const LOGIN_START = '[Auth] Login Start';
export const AUHTENTICATE_FAIL = '[Auth] Authenticate Fail';
export const AUHTENTICATE_SUCCESS = '[Auth] Authenticate Success';
export const LOGOUT = '[Auth] Logout';
export const CLEAR_ERROR = '[Auth] Clear Error';

export class SignUpStart implements Action {
    readonly type = SIGN_UP_START;
    constructor(public payload: AuthRequest) {}
}

export class LoginStart implements Action {
    readonly type = LOGIN_START;
    constructor(public payload: AuthRequest) {}
}

export class AuthenticateSuccess implements Action {
    readonly type = AUHTENTICATE_SUCCESS;
    constructor(public payload: {id: string, email: string, token: string, tokenExpiration: Date}) {}
}

export class AuthenticateFail implements Action {
    readonly type = AUHTENTICATE_FAIL;
    constructor(public payload: string) {}
}

export class Logout implements Action {
    readonly type = LOGOUT;
}

export class ClearError implements Action {
    readonly type = CLEAR_ERROR;
}

export type AuthActions = | SignUpStart | LoginStart | AuthenticateSuccess | AuthenticateFail | Logout | ClearError;
