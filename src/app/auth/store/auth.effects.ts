import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { switchMap, catchError, map, tap } from 'rxjs/operators';
import { AuthResponse } from '../auth.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as AuthActions from './auth.actions';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {

    @Effect()
    authSignUp = this.actions$.pipe(
        ofType(AuthActions.SIGN_UP_START),
        switchMap((authData: AuthActions.SignUpStart) => {
            return this.http.post<AuthResponse>(
                'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' + environment.fireBaseApiKey,
                authData.payload
            ).pipe(
                map(responseData => {
                    const tokenExpirationDate = new Date(new Date().getTime() + +responseData.expiresIn * 1000);
                    return new AuthActions.AuthenticateSuccess({
                        id: responseData.localId,
                        email: responseData.email,
                        token: responseData.idToken,
                        tokenExpiration: tokenExpirationDate
                    });
                }),
                catchError(errorResponse => {
                    return of(this.handleError(errorResponse));
                })
            );
        })
    );

    @Effect()
    authLogin = this.actions$.pipe(
        ofType(AuthActions.LOGIN_START),
        switchMap((authData: AuthActions.LoginStart) => {
            return this.http.post<AuthResponse>(
                'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' + environment.fireBaseApiKey,
                {
                    email: authData.payload.email,
                    password: authData.payload.password,
                    returnSecureToken: true
                }
            ).pipe(
                map(responseData => {
                    const tokenExpirationDate = new Date(new Date().getTime() + +responseData.expiresIn * 1000);
                    return new AuthActions.AuthenticateSuccess({
                        id: responseData.localId,
                        email: responseData.email,
                        token: responseData.idToken,
                        tokenExpiration: tokenExpirationDate
                    });
                }),
                catchError(errorResponse => {
                    return of(this.handleError(errorResponse));
                })
            );
        })
    );

    @Effect({dispatch: false})
    authRedirect = this.actions$.pipe(
        ofType(AuthActions.AUHTENTICATE_SUCCESS, AuthActions.LOGOUT),
        tap(() => {
            this.router.navigate(['/']);
        })
    );

    constructor(private actions$: Actions, private http: HttpClient, private router: Router) {}

    private handleError(errorResponse: HttpErrorResponse) {
        let errorMessage = 'An unknown error ocurred.';
        if (!errorResponse.error || !errorResponse.error.error) {
          return new AuthActions.AuthenticateFail(errorMessage);
        }
        switch (errorResponse.error.error.message) {
          case 'EMAIL_EXISTS':
            errorMessage = 'The email address is already in use by another account';
            break;
          case 'OPERATION_NOT_ALLOWED':
            errorMessage = 'Password sign-in is disabled for this project.';
            break;
          case 'TOO_MANY_ATTEMPTS_TRY_LATER':
            errorMessage = 'We have blocked all requests from this device due to unusual activity. Try again later.';
            break;
          case 'EMAIL_NOT_FOUND':
            errorMessage = 'There is no user record corresponding to this identifier. The user may have been deleted.';
            break;
          case 'INVALID_PASSWORD':
            errorMessage = 'The password is invalid or the user does not have a password.';
            break;
          case 'USER_DISABLED':
            errorMessage = 'The user account has been disabled by an administrator.';
            break;
          default:
            break;
        }
        return new AuthActions.AuthenticateFail(errorMessage);
    }

}
