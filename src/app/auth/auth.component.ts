import { Component, ViewChild, ComponentFactoryResolver, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from './auth.service';
import { AuthRequest, AuthResponse } from './auth.model';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AlertComponent } from '../shared/alert/alert.component';
import { PlaceholerDirective } from '../shared/placeholer.directive';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as AuthAction from './store/auth.actions';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit, OnDestroy {

  @ViewChild(PlaceholerDirective, {static: false}) alertHost: PlaceholerDirective;
  isLoginMode = true;
  isLoading = false;
  error = null;
  closeAlertSubscription: Subscription;
  storeSub: Subscription;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.storeSub = this.store.select('auth').subscribe(authState => {
      this.isLoading = authState.loading;
      this.error = authState.authError;
      if (this.error) {
        this.showErrorAlert(this.error);
      }
    });
  }

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const postData: AuthRequest = {
      email: form.value.email,
      password: form.value.password,
      returnSecureToken: true
    };
    if (this.isLoginMode) {
      this.store.dispatch(new AuthAction.LoginStart(postData));
    } else {
      this.store.dispatch(new AuthAction.SignUpStart(postData));
    }
    form.reset();
  }

  onAlertClose() {
    this.store.dispatch(new AuthAction.ClearError());
  }

  private showErrorAlert(message: string) {
    const alertFactory = this.componentFactoryResolver.resolveComponentFactory(AlertComponent);
    const alertContainerRef = this.alertHost.viewContainerRef;
    alertContainerRef.clear();
    const alertRef = alertContainerRef.createComponent(alertFactory);
    alertRef.instance.message = message;
    this.closeAlertSubscription = alertRef.instance.close.subscribe(() => {
      this.closeAlertSubscription.unsubscribe();
      alertContainerRef.clear();
    });
  }

  ngOnDestroy() {
    this.storeSub.unsubscribe();
    if (this.closeAlertSubscription) {
      this.closeAlertSubscription.unsubscribe();
    }
  }

}
