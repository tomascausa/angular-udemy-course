import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject } from 'rxjs';
import { AuthRequest, AuthResponse } from './auth.model';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { User } from '../shared/user.model';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as AuthActions from '../auth/store/auth.actions';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  error = new Subject<string>();
  expirationInstance: any;

  constructor(private http: HttpClient, private router: Router, private store: Store) { }

  autoLogin() {
    const user: {
      id: string,
      email: string,
      _token: string,
      _tokenExpiration: string
    } = JSON.parse(localStorage.getItem('user'));
    if (!user) {
      return;
    }
    const loadedUser = new User(user.id, user.email, user._token, new Date(user._tokenExpiration));
    if (loadedUser.token) {
      // this.user.next(loadedUser);
      this.store.dispatch(new AuthActions.AuthenticateSuccess({
        id: user.id,
        email: user.email,
        token: user._token,
        tokenExpiration: new Date(user._tokenExpiration)
      }));
      this.autoLogout(new Date(user._tokenExpiration).getTime() - new Date().getTime());
    } {
      return;
    }
  }

  logout() {
    this.store.dispatch(new AuthActions.Logout());
    // this.router.navigate(['/auth']);
    localStorage.removeItem('user');
    if (this.expirationInstance) {
      clearTimeout(this.expirationInstance);
    }
  }

  autoLogout(expirationDuration: number) {
    setTimeout(() => {
      this.logout();
    }, expirationDuration);
  }

}
