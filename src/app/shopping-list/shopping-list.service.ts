import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Ingredient } from '../shared/ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  ingredientsChanged = new Subject<Ingredient[]>();
  startEditing = new Subject<number>();

  ingredients: Ingredient[] = [
    new Ingredient(1, 'Apples', 5),
    new Ingredient(2, 'Tomatoes', 10),
  ];

  constructor() { }

  getIngredient(id: number) {
    const recipe = this.ingredients.find(
      (i) => {
        return i.id === id;
      }
    );
    return recipe;
  }

  getIngredients() {
    return this.ingredients.slice();
  }

  getIndex() {
    return this.ingredients[this.ingredients.length - 1].id;
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  addIngredients(ingredients: Ingredient[]) {
     this.ingredients.push(...ingredients);
     this.ingredientsChanged.next(this.ingredients.slice());
  }

  updateIngredient(id: number, newIngredient: Ingredient) {
    this.ingredients[this.ingredients.findIndex(i => i.id === id)] = newIngredient;
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  deleteIngredient(id: number) {
    this.ingredients.splice(this.ingredients.findIndex(i => i.id === id), 1);
    this.ingredientsChanged.next(this.ingredients.slice());
  }
}
