import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appPlaceholer]'
})
export class PlaceholerDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
