import { Directive, OnInit, ElementRef, Renderer2, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective implements OnInit {

  @HostBinding('class') class: string;
  toogle = true;

  constructor(private elRef: ElementRef, private rerender: Renderer2) { }

  ngOnInit(): void {
    this.class = '';
  }

  @HostListener('click') click(eventData: Event) {
    this.class = (this.toogle) ? 'open' : '';
    this.toogle = !this.toogle;
  }

}
