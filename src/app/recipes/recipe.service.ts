import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as ShoppingListActions from '../shopping-list/store/shopping-list.actions';


@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  recipesChanged = new Subject<Recipe[]>();

  private recipes: Recipe[] = [
    new Recipe(
        1,
        'A Test Recipe 1',
        'This is simply a test',
        'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg', 
        [
          new Ingredient(1, 'Meat', 1),
          new Ingredient(1, 'Oil', 10)
        ]
    ),
    new Recipe(
      2,
      'A Test Recipe 2',
      'This is simply a test',
      'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
      [
          new Ingredient(2, 'Spaghetti', 1)
      ]
    )
  ];

  constructor(
    private http: HttpClient,
    private store: Store<fromApp.AppState>
  ) {}

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(id: number) {
    const recipe = this.recipes.find(
      (r) => {
        return r.id === id;
      }
    );
    return recipe;
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.store.dispatch(new ShoppingListActions.AddIngredients(ingredients));
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(id: number, newRecipe: Recipe) {
    this.recipes[this.recipes.findIndex(i => i.id === id)] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(id: number) {
    this.recipes.splice(this.recipes.findIndex(i => i.id === id), 1);
    this.recipesChanged.next(this.recipes.slice());
  }

  getIndex() {
    return (this.recipes.length > 0) ? this.recipes[this.recipes.length - 1].id + 1 : 1;
  }

  storeRecipes() {
    return this.http.put('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]', this.getRecipes());
  }

  fetchRecipes() {
    return this.http.get<Recipe[]>(
      'https://angular-app-151ed.firebaseio.com/recipes.json',
      {
        responseType: 'json'
      }
    ).pipe(
      map(recipes => {
        return recipes.map(recipe => {
          return {
            ...recipe,
            ingredients: recipe.ingredients ? recipe.ingredients : []
          };
        });
      }),
      tap(recipes => {
        this.setRecipes(recipes);
      })
    );
  }

}
