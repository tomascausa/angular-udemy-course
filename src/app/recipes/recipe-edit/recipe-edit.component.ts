import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Recipe } from '../recipe.model';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  recipe: Recipe;
  editMode = false;
  form: FormGroup;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.recipe = this.recipeService.getRecipe(+params['id']);
        this.editMode = params['id'] != null;
        this.initForm();
      }
    );
  }

  onSubmit() {
    if (this.editMode) {
      this.recipeService.updateRecipe(this.recipe.id, this.form.value);
    } else {
      this.form.value['id'] = this.recipeService.getIndex();
      this.recipeService.addRecipe(this.form.value);
      this.router.navigate(['recipes', this.form.value['id']]);
    }
  }

  onAddIngredient() {
    (<FormArray>this.form.get('ingredients')).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
      })
    );
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  onDeleteIngredient(id) {
    (<FormArray>this.form.get('ingredients')).removeAt(id);
  }

  get controls() {
    return (<FormArray>this.form.get('ingredients')).controls;
  }

  private initForm() {
    const ingredients = new FormArray([]);
    if (this.editMode && this.recipe['ingredients']) {
      for (const ingredient of this.recipe.ingredients) {
        ingredients.push(
          new FormGroup({
            'name': new FormControl(ingredient.name, Validators.required),
            'amount': new FormControl(ingredient.amount, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
          })
        );
      }
    }
    this.form = new FormGroup({
      'id': new FormControl(this.editMode ? this.recipe.id : null, Validators.pattern(/^[1-9]+[0-9]*$/)),
      'name': new FormControl(this.editMode ? this.recipe.name : '', Validators.required),
      'description': new FormControl(this.editMode ? this.recipe.description : '', Validators.required),
      'imagePath': new FormControl(this.editMode ? this.recipe.imagePath : '', Validators.required),
      'ingredients': ingredients
    });
  }
}
